package ru.example.spring.tests.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public enum Scope {

    OPENID("openid");

    private final String name;
}
