package ru.example.spring.tests.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.example.spring.tests.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {
}
