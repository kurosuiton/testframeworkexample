package ru.example.spring.tests.requests;

import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.example.spring.tests.properties.CommonProperty;
import ru.example.spring.tests.properties.EndpointsProperty;

import static io.restassured.RestAssured.given;

@Component
@Slf4j
public class AuthRequest {

    @Autowired
    private CommonProperty commonProperty;
    @Autowired
    private EndpointsProperty endpointsProperty;

    @Step("Отправка запроса на получение токена")
    public Response postToken(String authHeader, String xFingerPrint, String login, String password, String grant_type
            , String scope, String otp, String transactionId, String sessionDataKey) {
        Response response = given().relaxedHTTPSValidation()
                .log().all()
                .filter(new AllureRestAssured())
                .baseUri(commonProperty.getUrl())
                .contentType(ContentType.URLENC)
                .header("Authorization", authHeader)
                .header("x-finger-print", xFingerPrint)
                .formParams("grant_type", grant_type,
                        "login", login,
                        "password", password,
                        "scope", scope,
                        "otp", otp,
                        "transactionId", transactionId,
                        "sessionDataKey", sessionDataKey)
                .post(endpointsProperty.getAuth().getToken());
        response.prettyPrint();
        return response;
    }
}
