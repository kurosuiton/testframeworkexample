package ru.example.spring.tests.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@Getter
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Error401 implements BodyModel {

    @JsonProperty(value = "additional_properties")
    private AdditionalProperties additionalProperties;

    @JsonProperty(value = "message")
    private String message;

    @JsonProperty(value = "message_title")
    private String messageTitle;

    @JsonProperty(value = "type")
    private String type;

    @SneakyThrows
    @Override
    public String toString() {
        return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
    }
}
