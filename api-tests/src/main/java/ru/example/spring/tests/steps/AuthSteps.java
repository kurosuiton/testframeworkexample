package ru.example.spring.tests.steps;

import io.qameta.allure.Step;
import io.restassured.internal.RestAssuredResponseImpl;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.example.spring.tests.context.TestContext;
import ru.example.spring.tests.enums.Domain;
import ru.example.spring.tests.enums.GrantType;
import ru.example.spring.tests.enums.Message;
import ru.example.spring.tests.enums.Scope;
import ru.example.spring.tests.models.AdditionalProperties;
import ru.example.spring.tests.models.Error401;
import ru.example.spring.tests.properties.CommonProperty;
import ru.example.spring.tests.requests.AuthRequest;

@Component
@Slf4j
public class AuthSteps extends BaseSteps {

    @Autowired
    private CommonProperty commonProperty;
    @Autowired
    private TestContext testContext;
    @Autowired
    private AuthRequest authRequest;

    @Step("Авторизуемся в Web по логину и паролю {login} / {password} в домене master")
    public AuthSteps loginWebByUnkAndPasswordInMasterDomainFirstFactor(String login, String password) {
        Response response = authByLoginAndPassword(commonProperty.getAuthHeaderWeb(), commonProperty.getXFingerPrint()
                , login, password, GrantType.LOGIN.getName(), Scope.OPENID.getName());

        checkStatusCode(401, response.getStatusCode());

        AdditionalProperties additionalProperties = AdditionalProperties.builder()
                .domain(Domain.MASTER.getName())
                .username(login).build();
        Error401 expectedBody = Error401.builder()
                .message(Message.SECOND_FACTOR_REQUIRED.getMessage())
                .type(Message.SECOND_FACTOR_REQUIRED.getType())
                .messageTitle(Message.SECOND_FACTOR_REQUIRED.getMessageTitle())
                .additionalProperties(additionalProperties).build();
        equalsResponse(expectedBody, response.getBody().as(Error401.class));

        return this;
    }

    @Step("Авторизуемся в Web по логину и паролю {login} / {password} с ОТП-кодом {otp} в домене master")
    public AuthSteps loginWebByUnkAndPasswordInMasterDomainSecondFactorOtp(String login, String password, String otp) {
        Error401 authResponse = testContext.fetch("authResponse", RestAssuredResponseImpl.class)
                .getBody().as(Error401.class);
        String transactionId = authResponse.getAdditionalProperties().getTransactionId();
        String sessionDataKey = authResponse.getAdditionalProperties().getSessionDataKey();

        Response response = authWithSecondFactor(login, password, otp, commonProperty.getAuthHeaderWeb()
                , commonProperty.getXFingerPrint(), GrantType.LOGIN.getName(), Scope.OPENID.getName()
                , transactionId, sessionDataKey);

        checkStatusCode(200, response.getStatusCode());

        return this;
    }

    @Step("Отправка запроса авторизации первого фактора для {login} / {password}")
    private Response authByLoginAndPassword(String authHeader, String xFingerPrint, String login, String password
            , String grantType, String scope) {
        log.info("Отправка запроса авторизации " + login + "/" + password);
        Response response = authRequest.postToken(authHeader, xFingerPrint, login, password, grantType, scope
                , null, null, null);
        testContext.store("authResponse", response);
        return response;
    }

    @Step("Отправка запроса авторизации второго фактора для {login} / {password} с ОТП-кодом {otp}")
    private Response authWithSecondFactor(String login, String password, String otp, String authHeader
            , String xFingerPrint, String grantType, String scope, String transactionId, String sessionDataKey) {
        log.info("Отправка запроса второго фактора авторизации " + login + "/" + password + " с ОТП-кодом " + otp);
        Response response = authRequest.postToken(authHeader, xFingerPrint, login, password, grantType, scope, otp
                , transactionId, sessionDataKey);
        testContext.store("authResponse", response);
        return response;
    }
}
