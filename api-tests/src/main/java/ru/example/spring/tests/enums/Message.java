package ru.example.spring.tests.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public enum Message {

    SECOND_FACTOR_REQUIRED("Требуется второй фактор.", "second_factor_required", "Произошла ошибка");

    private final String message;
    private final String type;
    private final String messageTitle;
}
