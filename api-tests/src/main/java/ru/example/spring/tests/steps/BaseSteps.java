package ru.example.spring.tests.steps;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.testng.AssertJUnit;
import ru.example.spring.tests.models.BodyModel;

@Slf4j
public abstract class BaseSteps {
    @Step("Проверка кода ответа {expectedCode}")
    protected void checkStatusCode(int expectedCode, int actualCode) {
        log.info("Проверка кода ответа " + expectedCode);
        AssertJUnit.assertEquals("Не верный код ответа"
                , expectedCode
                , actualCode);
    }

    @Step("Проверка тела ответа")
    protected void equalsResponse(BodyModel expectedBody, BodyModel actualBody) {
        log.info("Проверка тела ответа");
        AssertJUnit.assertEquals("Полученный ответ не соответствует ожидаемому"
                , expectedBody
                , actualBody);
    }
}
