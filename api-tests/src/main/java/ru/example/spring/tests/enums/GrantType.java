package ru.example.spring.tests.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public enum GrantType {

    LOGIN("login");

    private final String name;
}
