package ru.example.spring.tests;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.example.spring.tests.dao.UserRepository;
import ru.example.spring.tests.entity.User;
import ru.example.spring.tests.enums.Domain;
import ru.example.spring.tests.helpers.UncManager;

import java.util.List;
import java.util.Optional;

public class DBTest extends BaseTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UncManager uncManager;
    private String unc;

    @BeforeClass(description = "Подготовка тестовых данных")
    public void preCondition() throws Exception {
        unc = uncManager.getAvailableUnc();
    }

    @Test
    public void getUsers() {
        List<User> all = userRepository.findAll();
        System.out.println(all);
    }

    @Test
    public void getUserById() {
        User user = null;
        int id = Integer.parseInt(unc);
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            user = userOptional.get();
        }
        System.out.println(user);
    }

    @Test
    public void saveNewUser() {
        User user = User.builder().domain(Domain.MASTER.getName()).lock(0).mobilePhone("9997891245").build();
        user = userRepository.save(user);
        System.out.println(user);
    }

    @Test
    public void changeUserAlias() throws Exception {
        User user = null;
        int id = Integer.parseInt(unc);
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            user = userOptional.get();
        }
        assert user != null;
        user.setAlias("changed");
        user = userRepository.save(user);
        System.out.println(user);
    }

    @AfterClass(description = "Освобождаем клиента")
    public void postCondition() {
        uncManager.releaseUnc(unc);
    }
}
