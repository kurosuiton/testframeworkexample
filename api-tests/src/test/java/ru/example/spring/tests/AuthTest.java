package ru.example.spring.tests;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import io.qameta.allure.TmsLink;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.example.spring.tests.helpers.UncManager;
import ru.example.spring.tests.properties.CommonProperty;
import ru.example.spring.tests.steps.AuthSteps;

@Epic("Авторизация")
@Feature("УНК и пароль зарегистрированного клиента")
public class AuthTest extends BaseTest {

    @Autowired
    private CommonProperty commonProperty;
    @Autowired
    private AuthSteps authSteps;
    @Autowired
    private UncManager uncManager;

    private String unc;
    private String password;
    private String otp;

    @BeforeTest(description = "Подготовка тестовых данных")
    public void preCondition() throws Exception {
        unc = uncManager.getAvailableUnc();
        password = commonProperty.getPassword();
        otp = commonProperty.getOtp();
    }

    @Test(description = "Авторизация по логину и паролю")
    public void loginByUnkAndPassword() {
        authSteps.loginWebByUnkAndPasswordInMasterDomainFirstFactor(unc, password);
    }

    @Test(description = "Авторизация по логину и паролю со вторым фактором ОТП-кодом", dependsOnMethods = "loginByUnkAndPassword")
    @TmsLink("2b499518-a627-45e3-8ddb-bc8c876406b5")
    @Link(type = "manual", value = "cd077e8f-c4c7-4615-890b-17ffd6108c0a" /* 426346 */)
    public void loginByUnkAndPasswordWithSecondFactor() {
        authSteps.loginWebByUnkAndPasswordInMasterDomainFirstFactor(unc, password)
                .loginWebByUnkAndPasswordInMasterDomainSecondFactorOtp(unc, password, otp);
    }

    @AfterTest(description = "Освобождаем клиента")
    public void postCondition() {
        uncManager.releaseUnc(unc);
    }

}
